namespace SOLID.Services;

public sealed class EasyGameService : IGameService
{
    #region Private Field

    private readonly IConsoleService _consoleService;
    private readonly IGeneratorService _generatorService;
    private readonly IConfiguration _config;

    #endregion;

    #region Private Properties

    private int Number { get; set; }

    #endregion;

    #region Public Constructor

    public EasyGameService(
        IConsoleService consoleService,
        IGeneratorService generatorService,
        IConfiguration config)
    {
        _consoleService = consoleService;
        _generatorService = generatorService;
        _config = config;
    }

    #endregion

    #region Public Methods

    public void Start()
    {
        int attempts = _config.GetValue<int>("attempts");
        int min = _config.GetValue<int>("interval:min");
        int max = _config.GetValue<int>("interval:max");

        if (min >= max)
        {
            _consoleService.IntervalError();
        }
        else
        {
            int attempt = 0;
            bool success = false;

            _consoleService.PrintRules(min, max, attempts);
            Number = _generatorService.Generate(min, max);

            _consoleService.WaitPlayer();

            while (attempt < attempts)
            {
                attempt++;
                _consoleService.PrintAttempt(attempt);
                int Value = _consoleService.GetNumber(min, max);
                if (Value == Number)
                {
                    success = true;
                    _consoleService.PrintWin(attempt);
                    break;
                }
                else if (Value < Number)
                {
                    _consoleService.PrintMore();
                }
                else
                {
                    _consoleService.PrintLess();
                }
            }
            if (!success)
            {
                Console.WriteLine("\n����� ������� ��������");
                _consoleService.PrintLose(Number);
            }
        }

        #endregion
    }
}