﻿namespace SOLID.Services;

internal sealed class ConsoleHardGameService : ConsoleService, IConsoleService
{
    #region Public Methods

    public void PrintRules(int min, int max, int attempts)
    {
        Console.WriteLine("\nПРАВИЛА (сложный уровень):");
        Console.WriteLine($"\t* Необходимо угадать число из интервала [{min},{max}]");
        Console.WriteLine($"\t* Число попыток: {attempts}");
        Console.WriteLine("\t* Нужно успеть за 1 минуту");
    }

    public void WaitPlayer()
    {
        Console.WriteLine("\nДля начала игры нажмите любую клавишу...");
        Console.ReadLine();
        Console.WriteLine("Время пошло!");
    }

    #endregion;
}
