namespace SOLID.Services;

public sealed class GeneratorService : IGeneratorService
{
    public int Generate(int min, int max)
    {
        return new Random().Next(min, max+1);
    }
}