﻿namespace SOLID.Services;

internal sealed class StartService: IStartService
{
    #region Private Field

    private readonly IGameService _gameService;
    private static bool _isStarted;

    #endregion;

    #region Public Constructor

    public StartService(IGameService gameService)
    {
        _gameService = gameService;
    }

    #endregion

    #region Public Methods

    public void Start()
    {
        try
        {
            _isStarted = true;
            PrintWelcome();
            while (_isStarted)
            {
                switch (StartGameMenu())
                {
                    case "1":
                        _gameService.Start();
                        break;
                    case "2":
                        return;
                    default:
                        Console.WriteLine("Неизвестная команда. Попробуйте ввести еще раз.");
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Программа завершена сбоем.");
            Console.WriteLine($"EM: {ex.Message}.");
            Console.WriteLine($"ST: {ex.StackTrace}.");
        }
        finally
        {
            _isStarted = false;
        }
    }

    #endregion;

    #region Private Methods

    private void PrintWelcome()
    {
        Console.ForegroundColor = ConsoleColor.DarkGreen;
        Console.WriteLine("\nДобро пожаловать в игру \"УГАДАЙ ЧИСЛО\"");
        Console.ResetColor();
    }

    private string? StartGameMenu()
    {
        Console.WriteLine("\nВыберите действие:" +
        "\n\t1 - Начать новую игру" +
        "\n\t2 - Выйти");
        return Console.ReadLine();
    }

    #endregion;
}