namespace SOLID.Services;

public abstract class ConsoleService
{
    #region Public Methods

    public void PrintAttempt(int attempt)
    {
        Console.WriteLine("\n________________________");
        Console.WriteLine($"\n* * * ������� �: {attempt} * * *");
    }

    public void PrintWin(int attempt)
    {
        Console.ForegroundColor = ConsoleColor.DarkGreen;
        Console.WriteLine("�� �������!");
        Console.WriteLine($"���������� �������: {attempt}");
        Console.ResetColor();
    }

    public void PrintLose(int number)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("�� ���������!");
        Console.WriteLine($"���� �������� ����� {number}");
        Console.ResetColor();
    }

    public void PrintMore()
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(">>> ���������� ����� ������");
        Console.ResetColor();
    }

    public void PrintLess()
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("<<< ���������� ����� ������");
        Console.ResetColor();
    }

    public void IntervalError()
    {
        Console.WriteLine("\n������ ���������� ��������. ����� �������� ��������� ������ ���� ������ �������");
    }

    public int GetNumber(int min, int max)
    {
        reading:
        Console.WriteLine("\n������� �����:");
        if ((int.TryParse(Console.ReadLine(), out int input)) && ((input >= min) && (input <= max)))
        {
            return input;
        }
        else
        {
            Console.WriteLine($"���������� ������ ����� �� ��������� [{min},{max}]");
            Console.WriteLine("���������� ������ ��� ���");
            goto reading;
        }
    }

    #endregion;
}
