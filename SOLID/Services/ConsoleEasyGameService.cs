﻿namespace SOLID.Services;

internal sealed class ConsoleEasyGameService: ConsoleService, IConsoleService
{
    #region Public Methods

    public void PrintRules(int min, int max, int attempts)
    {
        Console.WriteLine("\nПРАВИЛА (простой уровень):");
        Console.WriteLine($"\t* Необходимо угадать число из интервала [{min},{max}]");
        Console.WriteLine($"\t* Число попыток: {attempts}");
    }

    public void WaitPlayer()
    {
        Console.WriteLine("\nДля начала игры нажмите любую клавишу...");
        Console.ReadLine();
    }

    #endregion;
}
