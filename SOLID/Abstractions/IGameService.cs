namespace SOLID.Abstractions;

public interface IGameService
{
    void Start();
}
