namespace SOLID.Abstractions;

public interface IConsoleService
{
    public int GetNumber(int min, int max);
    public void PrintRules(int min, int max, int attempts);
    public void WaitPlayer();
    public void IntervalError();
    public void PrintAttempt(int attempt);
    public void PrintWin(int attempt);
    public void PrintMore();
    public void PrintLess();
    public void PrintLose(int number);
}