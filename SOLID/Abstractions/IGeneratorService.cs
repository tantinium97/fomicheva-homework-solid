namespace SOLID.Abstractions;

public interface IGeneratorService
{
    public int Generate(int min, int max);
}