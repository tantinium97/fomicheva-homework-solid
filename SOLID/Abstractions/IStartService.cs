﻿using System;
namespace SOLID.Abstractions;

internal interface IStartService
{
    void Start();
}
