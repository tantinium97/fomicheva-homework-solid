﻿namespace SOLID;

static class Program
{
    #region Private Methods

    static void Main(string[] args)
    {
        IHost host = Host.CreateDefaultBuilder(args).Build();
        IConfiguration config = host.Services.GetRequiredService<IConfiguration>();

        var serviceCollection = new ServiceCollection();

        serviceCollection.AddSingleton(config);
        serviceCollection.AddSingleton<IGeneratorService, GeneratorService>();
        serviceCollection.AddSingleton<IStartService, StartService>();

        string level = config.GetValue<string>("level");
        bool isLevelHard = level == "hard";
        bool isLevelEasy = level == "easy";

        if (!isLevelHard && !isLevelEasy)
        {
            Console.WriteLine("Ошибка заполнения настроек. Необходимо заполнить поле 'level' значением 'hard' или 'easy'.");
            return;
        }

        if (isLevelHard)
        {
            serviceCollection.AddSingleton<IGameService, HardGameService>();
            serviceCollection.AddSingleton<IConsoleService, ConsoleHardGameService>();
        }
        else if (isLevelEasy)
        {
            serviceCollection.AddSingleton<IGameService, EasyGameService>();
            serviceCollection.AddSingleton<IConsoleService, ConsoleEasyGameService>();
        }

        IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
        var service = serviceProvider.GetService<IStartService>();
        service.Start();
    }

    #endregion;
}